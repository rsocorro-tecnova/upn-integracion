﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UPN_UTIL.IO;

namespace UPN_UTIL
{
    public static class Util
    {
        /// <summary>
        /// permite validar si una lista es vacia o nula
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<T>(List<T> list)
        {
            if (list != null && list.Count > 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Permite validar si una fecha tiene un valor valido o no
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(DateTime fecha)
        {
            if (fecha >= DateTime.MinValue)
            {
                return false;
            }
            return true;
        }

        public static bool IsNullOrEmpty(int value)
        {
            if (value > 0)
            {
                return false;
            }
            return true;
        }


        public static bool IsNullOrEmpty(decimal value)
        {
            if (value > 0)
            {
                return false;
            }
            return true;
        }

        public static T ConvertFromXml<T>(string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(xml))
            {
                var test = (T)serializer.Deserialize(reader);
                return test;
            }
        }

        public static string convertToXml<T>(T objeto)
        {
            using (var stringwriter = new Utf8StringWriter())
            {
                var serializer = new XmlSerializer(objeto.GetType());
                serializer.Serialize(stringwriter, objeto);
                return stringwriter.ToString();
            }

        }
        public static Y ConvertFromJson<Y>(string objeto)
        {
            return JsonConvert.DeserializeObject<Y>(objeto);
        }

        public static string ConvertToJson<T>(T objeto)
        {
            return JsonConvert.SerializeObject(objeto);
        }

    }
}
