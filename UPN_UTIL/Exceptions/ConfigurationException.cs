﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UPN_UTIL.Exceptions
{
    [Serializable]
    public class ConfigurationException : Exception, ISerializable
    {
        public ConfigurationException()
        {
            // Not ImplementedException
        }

        public ConfigurationException(string message) : base(message)
        {
            // Not ImplementedException
        }
        public ConfigurationException(string message, Exception innerException) : base(message, innerException)
        {
            // Not ImplementedException
        }
    }
}
