﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UPN_UTIL.Exceptions
{
    [Serializable]
    public class IntegrationException : Exception, ISerializable
    {
        public IntegrationException()
        {
            // Not ImplementedException
        }

        public IntegrationException(string message) : base(message)
        {
            // Not ImplementedException
        }
        public IntegrationException(string message, Exception innerException) : base(message, innerException)
        {
            // Not ImplementedException
        }
    }
}
