﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UPN_UTIL.Exceptions
{
    [Serializable]
    public class BusinessException : Exception, ISerializable
    {

        public BusinessException()
        {
            // Not ImplementedException
        }

        public BusinessException(string message) : base(message)
        {
            // Not ImplementedException
        }
        public BusinessException(string message, Exception innerException) : base(message, innerException)
        {
            // Not ImplementedException
        }
    }
}
