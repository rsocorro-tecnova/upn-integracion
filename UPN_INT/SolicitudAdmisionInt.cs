﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPN_DTO;
using UPN_UTIL.Exceptions;

namespace UPN_INT
{
    public class SolicitudAdmisionInt : BaseInt
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Solicitud de Admisión
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public static async Task<SolicitudAdmisionDto> EnviarSolicitudAdmision(SolicitudAdmisionDto solicitudAdmision)
        {
            log.Debug("EnviarSolicitudAdmision Init");
            try
            {
                string path = "";
                path = path.Replace("{param}", solicitudAdmision.Id.ToString());
                SolicitudAdmisionDto responseDto = await Post<SolicitudAdmisionDto>(path);
                return responseDto;
            }
            catch (Exception ex)
            {
                log.Error("Error en integracion.", ex);
                throw new IntegrationException("Error Inesperado.", ex);
            }
        }
    }
}
