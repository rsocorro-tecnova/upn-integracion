﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using UPN_UTIL;
using UPN_UTIL.Exceptions;

namespace UPN_INT
{
    public abstract class BaseInt
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // SOLICITUD ADMISION WEB SERVICES
        protected const string PATH_SOLICITUD_ADMISION = "";


        protected const string application_json = "application/json";
        protected const string error_integracion = "Error en integración.";
        protected BaseInt()
        {
            // not implemented
        }

        /// <summary>
        /// Permite obtener el path base del api
        /// </summary>
        /// <returns></returns>
        protected static string BasePath()
        {
            return ConfigurationManager.AppSettings["SolicitudAdmision.Api.BasePath"];
        }

        /// <summary>
        /// Permite realizar la operación HTTP Post y devolviendo un objeto tipo
        /// </summary>
        /// <typeparam name="T">Tipo de objeto con el que se trabaja</typeparam>
        /// <param name="path">Url de solicitud</param>
        /// <param name="requestDto">Objeto ha enviar en solicitud</param>
        /// <returns></returns>
        protected static async Task<T> Post<T>(string path, T requestDto)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);

            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpResponseMessage response = await InternalPost(fullPath, requestDto);
                if (response.IsSuccessStatusCode)
                {
                    string dataResponse = await response.Content.ReadAsStringAsync();
                    T objectResponse = JsonConvert.DeserializeObject<T>(dataResponse);
                    return objectResponse;
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: POST, data :[{ Util.ConvertToJson(requestDto)}]");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }

        protected static async Task<T> Post<T>(string path)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);

            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpResponseMessage response = await InternalPost(fullPath);
                if (response.IsSuccessStatusCode)
                {
                    string dataResponse = await response.Content.ReadAsStringAsync();
                    T objectResponse = JsonConvert.DeserializeObject<T>(dataResponse);
                    return objectResponse;
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: POST, data :null");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }

        /// <summary>
        /// Permite realizar una solicitud HTTP Post a una url
        /// </summary>
        /// <typeparam name="Y">Tipo de respuesta</typeparam>
        /// <typeparam name="T">Tipo de Solicitud</typeparam>
        /// <param name="path">Url de servicio</param>
        /// <param name="requestDto">Datos de solicitud</param>
        /// <returns></returns>
        protected static async Task<Y> Post<Y, T>(string path, T requestDto)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);

            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpResponseMessage response = await InternalPost(fullPath, requestDto);
                if (response.IsSuccessStatusCode)
                {
                    string dataResponse = await response.Content.ReadAsStringAsync();
                    Y responseValue = JsonConvert.DeserializeObject<Y>(dataResponse);
                    return responseValue;
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: POST, data :[{ Util.ConvertToJson(requestDto)}]");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }

        protected static async Task<bool> PostIsOk<T>(string path, T requestDto)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);

            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpResponseMessage response = await InternalPost(fullPath, requestDto);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: POST, data :[{ Util.ConvertToJson(requestDto)}]");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }

        private static async Task<HttpResponseMessage> InternalPost<T>(string path, T requestDto)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(application_json));
            client.Timeout = TimeSpan.FromMinutes(3);
            StringContent dataRequest = null;
            if (requestDto != null)
            {
                dataRequest = new StringContent(JsonConvert.SerializeObject(requestDto), Encoding.UTF8, application_json);
            }
            return await client.PostAsync(path, dataRequest);
        }
        private static async Task<HttpResponseMessage> InternalPost(string path)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(application_json));
            client.Timeout = TimeSpan.FromMinutes(3);
            return await client.PostAsync(path, null);
        }

        /// <summary>
        /// Permite realizar una operacion HTTP PUT tipada
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="path">Url de servicio</param>
        /// <param name="requestDto">Objeto con datos a enviar</param>
        /// <returns></returns>
        protected static async Task<T> Put<T>(string path, T requestDto)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);
            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(application_json));

                var dataRequest = new StringContent(JsonConvert.SerializeObject(requestDto), Encoding.UTF8, application_json);
                HttpResponseMessage response = await client.PutAsync(fullPath, dataRequest);
                if (response.IsSuccessStatusCode)
                {
                    string dataResponse = await response.Content.ReadAsStringAsync();
                    T responseDto = JsonConvert.DeserializeObject<T>(dataResponse);
                    return responseDto;
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: PUT, data :[{ Util.ConvertToJson(requestDto)}]");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }

        /// <summary>
        /// Permite realizar una solicitud HTTP GET tipada
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="path">Url de servicio</param>
        /// <returns></returns>
        protected static async Task<T> Get<T>(string path)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);
            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(application_json));

                HttpResponseMessage response = await client.GetAsync(fullPath);
                if (response.IsSuccessStatusCode)
                {
                    string dataResponse = await response.Content.ReadAsStringAsync();
                    T responseDto = JsonConvert.DeserializeObject<T>(dataResponse);
                    return responseDto;
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: GET ");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }

        /// <summary>
        /// Permite realizar una operación HTTP GET tipada
        /// </summary>
        /// <typeparam name="T">Tipo</typeparam>
        /// <param name="path">Url de Servicio</param>
        /// <returns></returns>
        protected static async Task<List<T>> GetList<T>(string path)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);
            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(application_json));

                HttpResponseMessage response = await client.GetAsync(fullPath);
                if (response.IsSuccessStatusCode)
                {
                    string dataResponse = await response.Content.ReadAsStringAsync();
                    List<T> responseDto = JsonConvert.DeserializeObject<List<T>>(dataResponse);
                    return responseDto;
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: GET ");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }

        /// <summary>
        /// Permite realizar una operación HTTP DELETE tipada
        /// </summary>
        /// <typeparam name="T">TIpo</typeparam>
        /// <param name="path">Url de servicio</param>
        /// <returns></returns>
        protected static async Task<T> Delete<T>(string path)
        {
            string fullPath = string.Format("{0}{1}", BasePath(), path);
            if (!string.IsNullOrEmpty(fullPath))
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(application_json));

                HttpResponseMessage response = await client.DeleteAsync(fullPath);
                if (response.IsSuccessStatusCode)
                {
                    throw new IntegrationException(response.ReasonPhrase);
                }
                else
                {
                    log.Error("=========== DETAIL ========");
                    log.Error($"Path: {fullPath}, Method: DELETE ");
                    throw new IntegrationException(response.ReasonPhrase);
                }
            }
            else
            {
                throw new IntegrationException("Parametro de servicio no configurado.");
            }
        }
    }
}
