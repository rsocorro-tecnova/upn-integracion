﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using UPN_Integrations.Models;
using Swashbuckle.Swagger.Annotations;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;
using UPN_DTO;
using UPN_BLL;
using UPN_UTIL.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UPN_Integrations.Controllers
{
    //[ApiController]
    [RoutePrefix("api/banner")]
    //[Route("api/[controller]")]
    public class BannerController : ApiController
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// [integración sistema banner] solicitud de admisión
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("enviar-solicitud-admision")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(EntityResponse))]
        public async Task<IHttpActionResult> EnviarSolicitudAdmision(SolicitudAdmisionDto solicitudAdmision)
        {
            log.Debug("Solicitud Admision Init");

            EntityResponse response = new EntityResponse();
            try
            {
                if (solicitudAdmision != null)
                {
                    response.Result =  await SolicitudAdmisionBll.EnviarSolicitud(solicitudAdmision);
                    response.Code = (int)HttpStatusCode.OK;
                    response.Message = "La solicitud de admisión se ha enviado con éxito.";
                }

                else

                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                    response.save = false;
                    response.Message = "Solicitud no válida";
                }

                return (IHttpActionResult)Ok(response);
            }

            catch (BusinessException be)
            
            {
                response.Code = (int)HttpStatusCode.BadRequest;
                response.Message = be.Message;
                response.save = false;
                return (IHttpActionResult)Ok(response);
            }

            catch (Exception ex)
            
            {
                log.Error(ex);
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
                response.save = false;
                return (IHttpActionResult)Ok(response);
            }
        }

    }
}
