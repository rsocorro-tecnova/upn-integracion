﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPN_Integrations.Models
{
    public class EntityResponse : BaseResponse
    {
        public dynamic Result { get; set; }
    }
}
