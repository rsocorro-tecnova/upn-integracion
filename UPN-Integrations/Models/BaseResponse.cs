﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPN_Integrations.Models
{
    public class BaseResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public bool save { get; set; }
        public int IdOportunidad { get; set; }
    }
}
