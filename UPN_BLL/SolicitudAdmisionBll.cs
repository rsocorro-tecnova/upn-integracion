﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPN_DTO;
using UPN_INT;
using UPN_UTIL.Exceptions;

namespace UPN_BLL
{
    public static class SolicitudAdmisionBll
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string errorGeneral = "Error al enviar los datos de la solicitud de admisión. Por favor, contacte al administrador.";
        /// <summary>
        /// Solicitud de Admisión
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// 
        public static async Task<SolicitudAdmisionDto> EnviarSolicitud(SolicitudAdmisionDto solicitudAdmision)
        {
            log.Debug("Enviar Solicitud de Admisión  Init");
            try
            {
                return await SolicitudAdmisionInt.EnviarSolicitudAdmision(solicitudAdmision);
            }
            catch (IntegrationException dx)
            {
                throw new BusinessException("Error al enviar solicitud de admisión", dx);
            }
            catch (Exception ex)
            {
                log.Error("Error inesperado.", ex);
                throw new BusinessException("Error inesperado.", ex);
            }
        }

    }
}
